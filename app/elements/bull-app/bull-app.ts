namespace Bulletins {

	/**
	 * Sample starter application. Rename this class and namespace appropriately and add any application-specific functionality here.
	 * The base class will add a new instance of this class to the window object under the name 'app'.
	 * Refer to the base classes ({@link NowElements.BaseApp} and {@link NowElements.BasicApp}) to understand the default functionality.
	 *
	 * @author Kito D. Mann
	 */
	@component('bull-app')
	export class App extends NowElements.BasicApp {
		/**
		 * The header text
		 * @type {String}
		 */
		@property({
			type: String,
			value: null
		})
		appHeader:string;
		// #tag::settings-property[]
		/**
		 * The settings object
		 * @type {Object}
		 */
		@property({
			type: Object,
			notify: true
		})
		settings:any;
		// #end::settings-property[]
		/**
		 * The route object from now-basic-app-layout
		 * @type {Object}
		 */
		@property({
			type: Object,
			notify: true
		})
		route:any;
		/**
		 * The subroute object from now-basic-app-layout
		 * @type {Object}
		 */
		@property({
			type: Object,
			notify: true
		})
		subroute:any;

		// #tag::get-settings[]
		/**
		 * The url to the settings file
		 * @type {Object}
		 */
		_getSettingsUrl() {
			var url = 'settings.json';
			var port = location.port;
			// Setup for running local
			if (port && port !== '80' && port !== '443') {
				url = '../../settings.json';
			}
			return url;
		}
		// #end::get-settings[]

		ready() {
			super.ready();
		}

		attached() {
			super.attached();
		}
		/**
		 * Set the header text
		 * @param {String} headerText The text for the header
		 */
		setHeader(headerText) {
			this.set('appHeader', headerText);
		}

	}
}

Bulletins.App.register();

