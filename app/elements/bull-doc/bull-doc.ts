namespace Bulletins {

	// If something is declared as a module, this is a way to override that module. You're basically
	// saying I know this exists so just trust me
	declare var quotedPrintable;

	@component('bull-doc')
	export class BullDoc extends NowElements.BaseView {
		/**
		 * Boolean for controlling the spinner
		 * @type {Boolean}
		 */
		@property({
			type: Boolean,
			notify: true
		})
		mainLoading:boolean;
		/**
		 * The settings object
		 * @type {Object}
		 */
		@property({
			type: Object
		})
		settings:any;
		/**
		 * The UNID of the document to load
		 * @type {String}
		 */
		@property({
			type: String,
			notify: true,
			observer: '_onSelectedDoc'
		})
		selectedDoc:string;
		/**
		 * The document object from DDS
		 * @type {Object}
		 */
		@property({
			type: Object,
			observer: '_onDoc'
		})
		doc:any;
		/**
		 * The content of the single rich text field. You may want to make this it's own
		 * function for multiple rich text fields.
		 * @type {String}
		 */
		@property({
			type: String
		})
		richTextContent:string;
		/**
		 * Fired when the selectedDoc property changes. Generates the request for the document (doc)
		 * @param {String} newVal The new value
		 * @param {String} oldVal The old value
		 */
		private _onSelectedDoc(newVal, oldVal) {
			if (newVal) {
				let ajax = this.$.docAjax;
				ajax.generateRequest();
			}
		}
		/**
		 * Fired when the doc property changes. Decodes the rich text field ('BulletinText') and populates
		 * the richTextContent property
		 * @param {Object} newVal The new value of doc
		 * @param {Object} oldVal The old value of doc
		 */
		private _onDoc(newVal, oldVal) {
			if (newVal) {
				let textObj = newVal.BulletinText.content;
				for (let i = 0; i < textObj.length; i++) {
					let mimeObj = textObj[i];
					if (mimeObj.contentType.indexOf('text/html') > -1) {
						let html = mimeObj.data;
						if (mimeObj.contentTransferEncoding === 'base64') {
							html = atob(mimeObj.data);
						}else if (mimeObj.contentTransferEncoding === 'quoted-printable') {
							html = quotedPrintable.decode(mimeObj.data);
						}
						let removeText = ['<html>','</html>','<body>','</body>','<head>','</head>'];
						for (let j = 0; j < removeText.length; j++) {
							html = html.replace(removeText[j],'');
						}
						this.set('richTextContent', html);
					}
				}
				// Gotta wait for the DOM to be built
				this.async(() => {
					this._updateImages(textObj);
				}, 200);
			}
		}
		/**
		 * Take the base64 from any image objects and set it as the src of the images
		 * @param {Array} mimeObjContent The mimeObj.content
		 */
		private _updateImages(mimeObjContent) {
			for (let j = 0; j < mimeObjContent.length; j++) {
				let mimeObj = mimeObjContent[j];
				if (mimeObj.contentType.indexOf('image/') > -1) {
					let contentDispo = mimeObj.contentDisposition;
					let isInline = contentDispo.indexOf('inline') > -1;
					if (isInline) {
						let imageBase64 = mimeObj.data;
						let imgElemName = 'cid:' + mimeObj.contentID.substring(1, mimeObj.contentID.length - 1);
						let contentType = mimeObj.contentType.split(';')[0];
						let elem = document.querySelector('[src="' + imgElemName + '"]');
						if (elem) {
							let srcAttrStr = 'data:' + contentType + ';base64,' + imageBase64;
							elem.setAttribute('src', srcAttrStr);
						}
					}else { // Gotta create an img tag for attachments
						let elemArr = document.querySelectorAll('i');
						for (let k = 0; k < elemArr.length; k++) {
							let elem = elemArr[k];
							let dispoArr = mimeObj.contentDisposition.split(';');
							let fileNamePart = dispoArr[1].substring(dispoArr[1].indexOf('="') + 2, dispoArr[1].lastIndexOf('"'));
							if (elem.innerHTML.indexOf(fileNamePart) > -1) {
								let parent = elem.parentNode;
								let img = document.createElement('img');
								let contentTypeArr = mimeObj.contentType.split(';');
								let srcAttrStr = 'data:' + contentTypeArr[0] + ';base64,' + mimeObj.data;
								img.setAttribute('src', srcAttrStr);
								parent.replaceChild(img, elem);
							}
						}
					}
				}
			}
		}
		/**
		 * Determine the URL for the document. Must wait for the settings and selectedDoc
		 * @param {Object} settings The settings object
		 * @param {String} docUnid  The docoument UNID
		 * @return {String}
		 */
		private _getUrl(settings, docUnid) {
			var url = null;
			var prefix = settings.NSF_URL;
			var mid = '/api/data/documents/unid/';
			var suffix = docUnid;
			return prefix + mid + suffix;
		}
	}
}

Bulletins.BullDoc.register();
