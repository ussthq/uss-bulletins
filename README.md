# (ARCHIVED: NON-PRODUCTION CODE ONLY) 
This was used as a training excercise, should not be used as basis for production code.

# Red Pill Polymer Starter Kit

This template is a starting point for building apps using a drawer-based
layout, and is based on the [Polymer Starter Kit](https://github.com/PolymerElements/polymer-starter-kit). 
It also includes [PolymerTS](https://github.com/nippur72/PolymerTS) for TypeScript integration.

The layout is provided by `app-layout` elements.

Items in this template include (other than the features included in the starter):

* Spinner for Drawer and Content area
* Footer with Feedback Button and Copyright
* Red Pill Theme. To change colors, just change the color palette
* neon-animated-pages with the now-animated-pages-behavior to control transitions
* Search Bar with voice support
* paper-toast element for user notifications
* Content scroll position control

## Background Info

Polymer is a library built on top of Web Components, which is a new set of 
HTML5 specifications. If you're new to Polymer and 
web components, the links below should help:

* [knowesis.io](http://knowesis.io/web/webcomponents) - The links here are a good starting point for general web components stuff. 
* [Java EE MVC and Web Components presentation (long version](https://www.youtube.com/watch?v=gg5CyKpE7zo) - The presentation is actually a good primer 
(you can skip the Java EE MVC stuff; our app has no back-end web framework).
* [Polymer site](https://www.polymer-project.org/) - Main Polymer site; has demos of the Paper and Iron elements, which we use, as well as general documentation.
* [Polymer Patterns](http://patternsinpolymer.com/) - Good book.
* [Polycasts](https://www.youtube.com/playlist?list=PLmccrGoe3AUDPa-DeKlVimmrkU5gUlImC) - Short videos by a Google Developer Advocate aboout Polymer 
(make sure you only pick one that are for Polymer 1.x).

TypeScript is a superset of JavaScript that includes ES6/ES2015 features as well as additional language constructs and type saftey. Here are some links to get you started:

* [TypeScript Primer](https://blog.logentries.com/2016/02/typescript-language-primer/)
* [TypeScript Home page](http://www.typescriptlang.org/) home page.

## Setup

First, make sure you have [Node.js](https://nodejs.org/) installed, so we can use the Node package manager (NPM).
Next, install the other key tools: 

* [Bower](http://bower.io/) - dependency management 
* [Gulp](http://gulpjs.com/) - build
* [TypeScript](http://www.typescriptlang.org/) - TypeScript compiler
* [Typings](https://github.com/typings/typings) - type definition manager for TypeScript
* [web-component-tester](https://github.com/Polymer/web-component-tester) - (wct) - testing

You can install them with these commands:

`[sudo] npm install --global gulp bower typescript typings web-component-tester`

Next, install dependencies that are managed by NPM:

`[sudo] npm install`

Install dependencies that are managed by Bower:

`bower install`

Install the TypeScript type definitions:

`typings install`

## Building and Running

This project is built using Gulp; the file `gulpfile.js` contains several build tasks. 
Many IDEs and text editors have Gulp integration, so look for integration in your tool of choice.

You can also run Gulp from the command line; here are some common tasks:

`gulp serve` - Builds the app and runs a local web server at port 5000 that will automatically reload changes you make on disk in the browser.
After you run this command, just open your browser to `http://localhost:5000/porta/`. The app should refresh automatically when you change the source.

`gulp` - Default target; builds the app (vulcanizing it, etc.) and places it in the `dist` folder.

`serve:dist` Runs the default target and then runs the server on port 5001. This is the production build with "vulcanized" files. 

You can see the other tasks (such as `clean`) in the `gulpfile.js`.

## Building your app

You build your application by writing new Polymer Elements. You write an 
element for each view, and also smaller pieces of your application that need 
specialized (or reusable) functionality, such as a search bar, navigation bar, 
dialog, etc.

Each element should be in its own folder and have a `.html` file plus a 
corresponding TypeScript (`.ts` file).

To create a new element, just copy one of the folders with sample elements from the 
`app/elements` folder in this project: `now-view1` for a page, and 
`now-footer` for a regular element. 

> NOTE: All elements should be in the `app/elements` folder. 
> NOTE: Prefix all application-specific elements with a short name for your application. 
For example, if your app is called "PropertyManager", you might have a component called 
`prop-navigation`. You may want to rename `now-app` to use the prefix for your app (i.e. `prop-app`) 

## Testing

Testing in this project is handled via Polymer's web-component-tester which uses several technologies 
(Selenium, Mocha, and Chai, to name a few) to run tests against several different browsers. 
See the [web-component-tester home page](https://github.com/Polymer/web-component-tester) for more information about these tools.

Tests for this project should be written in TypeScript instead of JavaScript.

### Writing Tests

The tests are located in `app/test`. See that folder for examples.

Here are the basic steps for writing a new test (for a specific custom element):

1. Copy an existing test folder, which should include an `.html` and `.ts` file.

1. Edit the `.html` file to load your element and load the proper `.js` file. 

1. Edit the `.ts` file to write your tests.

   Chai supports both a classic "assert" style of writing assertions, and a behavior-driven "expect" style. 
   We prefer the [expect style](http://chaijs.com/guide/styles/#expect).

   __Writing Page Tests__
   
   In order to test elements that are pages (such as `now-view1`), use the 
   [Page Objects pattern](https://github.com/SeleniumHQ/selenium/wiki/PageObjects) in order to encapsulate any DOM manipulation or 
   manipulation of the component's internal details. (The example in the link is in Java, but the same principles apply.)
   
   There is a base class called `BasePageTester` you should subclass for your page tests. 
   See either of the sample tests to see how this used. 
   (If your element is pretty simple, there is no need for the extra object).
1. Add references to your test html file in `app/test/index.html`. 

   >TIP: During development for testing, you can comment out the other tests listed in the suite.
   
1. Run `gulp serve` and develop your test using the browser of your choice by pointing to 
`http://localhost:5000/test/my-element/my-element-tests.html`. 
You must view the console in order to see the test results. (The pages should refresh automatically when you change the source.)

> TIP: you can also run the whole test suite by accessing: `http://localhost:5000/test/index.html` 

### Running Tests

[//]: # (#### Setup)

[//]: # (Before you can run your test against multiple browsers using web-component-tester (wct), there are two additional setup steps:)

[//]: # (If you're on a Mac, double-click on the file `bin/SafariDriver.safariextz` to install the Selenium Safari extension. )
[//]: # (If you're using a version of Firefox higher than 41, _downgrade_ to Firefox 41. If you're on a Mac, you can get it from the '/bin' folder.)

#### Running the test task

In order to run the test suite against multiple browsers (generally all of 
the browsers on your system), run the `gulp test` task.

### Testing Resources

The following links should give you a better understanding of how to write 
tests using web-component-tester, Mocha, and Chai.

> NOTE: Please follow the installation directions above instead of the steps 
covered in these documents. Also note that we're using TypeScript, not pure JavaScript for our tests.
 
* [Testing Web Components section in Polymer documentation](https://www.polymer-project.org/1.0/tools/tests.html)
* [Polymer Unit Testing](https://medium.com/google-developer-experts/polymer-unit-testing-d6a69910dc31)
  
## Deployment

* The `dist` folder needs to be copied to a folder that is exposed via Apache httpd. It's all static HTML, JS, and CSS.

